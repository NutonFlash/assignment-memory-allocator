#include "mem.h"
#include "test.h"

extern void (*test_func[TEST_COUNT])(void*);
extern const char* const test_descriptions[TEST_COUNT];

int main() {
    void* heap_start = heap_init(HEAP_INITIAL_SIZE);
    for (uint8_t i = 0; i < TEST_COUNT; i++) {
        before_test(i + 1, test_descriptions[i], heap_start);
        test_func[i](heap_start);
        after_test(i + 1, heap_start);
    }
    debug("Great! All test passed!\n");
    return 0;
}