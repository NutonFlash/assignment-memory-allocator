#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include "util.h"
#include <errno.h>
#include <unistd.h>

static bool occupy_memory(void* start_addr, size_t length) {
    void* result = mmap(start_addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE , 0, 0);
    if (result != MAP_FAILED) {
        if (result == start_addr) {
            debug("same, occupied is correct\n");
            return true;
        } else { // for local machine which has no access to MAP_FIXED_NOREPLACE
            debug("expect: %p, actual: %p\n", start_addr, result);
            return false;
        }
    }
    else {return false;}
}

static void* void_pointer_plus(void* pointer, int64_t summand) {
    return (void*) ((uint8_t*) pointer + summand);
}

void before_test(uint8_t number, const char* const description, void* start_heap) {
    debug("Start Test №%" PRIu8 "...\n", number);
    debug("Description:\n%s\n", description);
    debug("Heap before start:\n");
    debug_heap(stderr, start_heap);
    debug("Run!\n");
}

void after_test(uint8_t number, void* start_heap) {
    debug("Heap after test\n");
    debug_heap(stderr, start_heap);
    debug("Test №%" PRIu8 " finish success!\n\n\n", number);
}

void test1_free_heap_more_that_query_when_map_block_successful(void* start_heap) {
    size_t content_capacity = 300;
    void* new_content = _malloc(content_capacity);
    if (!new_content) { err("Test №1 failed: _malloc return null\n"); }
    debug("Heap after _malloc:\n");
    debug_heap(stderr, start_heap);
    struct block_header* header = block_get_header(new_content);
    if (header->is_free) { err("Test №1 failed: not set false to is_free\n"); }
    if (header->capacity.bytes < content_capacity) { err("Test №1 failed: alloc too little block\n"); }
    if (header->capacity.bytes < HEAP_INITIAL_SIZE) {
        if (!header->next || !header->next->is_free) { err("Test #1 failed: blocks didn't split, but may do it\n"); }
    }
    debug("Memory allocated corrected!\n");
    _free(new_content);
}

void test2_several_blocks_mapped_and_free_to_one_when_only_one_free(void* start_heap) {
    size_t content_capacity = 200;
    void* first_content = _malloc(content_capacity);
    void* second_content = _malloc(content_capacity);
    void* third_content = _malloc(content_capacity);
    struct block_header* first_header = block_get_header(first_content);
    struct block_header* second_header = block_get_header(second_content);
    struct block_header* third_header = block_get_header(third_content);
    if (!first_content || !second_content || !third_content) { err("Test №2 failed: _malloc return null\n"); }
    debug("Alloc successful! Heap after _malloc:\n");
    debug_heap(stderr, start_heap);
    _free(second_content);
    debug("Heap after _free:\n");
    debug_heap(stderr, start_heap);
    if (!second_header->is_free) { err("Test №2 failed: second block not freed!"); }
    if (first_header->is_free || third_header->is_free) { err("Test №2 failed: first|third block freed!"); }
    debug("Memory free corrected!\n");
    _free(third_content);
    _free(first_content);
}

void test3_several_blocks_mapped_and_free_to_two_when_only_two_free(void* start_heap) {
    size_t content_capacity = 200;
    void* first_content = _malloc(content_capacity);
    void* second_content = _malloc(content_capacity);
    void* third_content = _malloc(content_capacity);
    struct block_header* first_header = block_get_header(first_content);
    struct block_header* second_header = block_get_header(second_content);
    struct block_header* third_header = block_get_header(third_content);
    if (!first_content || !second_content || !third_content) { err("Test №3 failed: _malloc return null\n"); }
    debug("Alloc successful! Heap after _malloc:\n");
    debug_heap(stderr, start_heap);
    _free(second_content);
    _free(first_content);
    debug("Heap after _free:\n");
    debug_heap(stderr, start_heap);
    if (!first_header->is_free) { err("Test №3 failed: first block not freed!"); }
    if (third_header->is_free) { err("Test №3 failed: third block freed!"); }
    if ((first_header->next == second_header) || (first_header->next != third_header)) { err("Test №3 failed: first and second block not correct merged!"); }

    debug("Memory free corrected!\n");
    _free(third_content);
}

void test4_heap_not_enough_and_free_near_when_grow_heap_near_it(void* start_heap) {
    size_t content_capacity = HEAP_INITIAL_SIZE + 10;
    void* new_content = _malloc(content_capacity);
    if (!new_content) { err("Test №4 failed: _malloc return null\n"); }
    debug("Heap after _malloc:\n");
    debug_heap(stderr, start_heap);
    struct block_header* header = block_get_header(new_content);
    if (header->is_free) { err("Test №4 failed: not set false to is_free\n"); }
    if (header->capacity.bytes < content_capacity) { err("Test №4 failed: alloc too little block\n"); }
    if ((void *) header > void_pointer_plus(start_heap, HEAP_INITIAL_SIZE)) { err("Test №4 failed: block not start in old heap"); }
    debug("Memory allocated corrected!\n");
    _free(new_content);
}

void test5_heap_not_enough_and_not_free_near_when_grow_heap_with_skip(void* start_heap) {
    bool result = occupy_memory(void_pointer_plus(HEAP_START, HEAP_INITIAL_SIZE * 3), getpagesize());
    if (!result) { err("Test №5 failed: can't occupied memory near first heap\n"); }
    size_t content_capacity = 30000;
    void* new_content = _malloc(content_capacity);
    if (!new_content) { err("Test №5 failed: _malloc return null\n"); }
    debug("Heap after _malloc:\n");
    debug_heap(stderr, start_heap);
    struct block_header* header = block_get_header(new_content);
    if (header->is_free) { err("Test №5 failed: not set false to is_free\n"); }
    if (header->capacity.bytes < content_capacity) { err("Test №5 failed: alloc too little block\n"); }
    if ((void *) header < void_pointer_plus(start_heap, HEAP_INITIAL_SIZE * 3)) { err("Test №5 failed: block start in old heap\n"); }
    debug("Memory allocated corrected!\n");
    _free(new_content);
}

void (*test_func[TEST_COUNT])(void*) = {
        test1_free_heap_more_that_query_when_map_block_successful,
        test2_several_blocks_mapped_and_free_to_one_when_only_one_free,
        test3_several_blocks_mapped_and_free_to_two_when_only_two_free,
        test4_heap_not_enough_and_free_near_when_grow_heap_near_it,
        test5_heap_not_enough_and_not_free_near_when_grow_heap_with_skip
};

const char* const test_descriptions[TEST_COUNT] = {
        "This test checks when memory is requested less than the heap.",
        "This test checks when _free is called on the second of three consecutive occupied blocks, the second block is freed, and the first and third remain occupied.",
        "This test checks when calling _free on two blocks located one after the other, they are merged, and the remaining blocks are not freed.",
        "This test checks when requesting a block with a size larger than the heap, and with further free memory, it expands without breaks.",
        "This test checks when a block with a size larger than the heap is requested, and when further memory is occupied, the heap expands with a break."
};
