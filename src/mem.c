#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header* block) { 
  return block->capacity.bytes >= query; 
}

static size_t pages_count(size_t mem) { 
  return mem / getpagesize() + ((mem % getpagesize()) > 0); 
}

static size_t round_pages(size_t mem) { 
  return getpagesize() * pages_count(mem); 
}

static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static struct block_header* create_block(void* restrict addr, block_size size, void* restrict next ) {
    block_init(addr, size, next);
    return (struct block_header*) addr;
}

static struct region create_region(void* addr, const size_t sz, bool extends) {
    return (struct region) {
        .addr = addr,
        .size = sz,
        .extends = extends
    };
}

static size_t region_actual_size(size_t query) { 
  return size_max(round_pages(query), REGION_MIN_SIZE); 
}

extern inline bool region_is_invalid(const struct region* r);


static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const* addr, size_t query) {
    bool extends = true;
    size_t actual_query = region_actual_size(size_from_capacity((block_capacity) {query}).bytes);
    void* addr_result = map_pages(addr, actual_query, MAP_FIXED_NOREPLACE);
    if (addr_result == MAP_FAILED) {
        extends = false;
        addr_result = map_pages(addr, actual_query, 0);
        if (addr_result == MAP_FAILED) { return REGION_INVALID; }
    }
    block_init(addr_result, (block_size) {actual_query}, NULL);
    return create_region(addr_result, actual_query, extends);
}

static void* block_after(struct block_header const* block);         

void* heap_init(size_t initial) {
  const struct region allocated_region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&allocated_region)) return NULL;
  return allocated_region.addr;
}

#define BLOCK_MIN_CAPACITY 24

static size_t block_actual_capacity(size_t query) {return size_max(BLOCK_MIN_CAPACITY, query);}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header* restrict block, size_t query) {
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    if (block_splittable(block, query)) {
        block_size size_new_block = (block_size) {block->capacity.bytes - query};
        block->capacity.bytes = query;
        void* start_new_block = block_after(block);
        struct block_header* new_block = create_block(start_new_block, size_new_block, block->next);
        block->next = new_block;
        return true;
    } else {return false;}
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after(struct block_header const* block)              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(struct block_header const* first, struct block_header const* second) {
  return (void*)second == block_after(first);
}

static bool mergeable(struct block_header const* restrict first, struct block_header const* restrict second) {
  return first->is_free && second->is_free && blocks_continuous(first, second);
}

static bool try_merge_with_next(struct block_header* block) {
    struct block_header* second_block = block->next;
    if (second_block != NULL && mergeable(block, second_block)) {
        block->capacity.bytes = block->capacity.bytes + size_from_capacity(second_block->capacity).bytes;
        block->next = second_block->next;
        return true;
    } else {return false;}
}


/*  --- ... eсли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz){
    struct block_header* cur_block = block;
    struct block_search_result result = (struct block_search_result) {
            .block = block,
            .type = BSR_REACHED_END_NOT_FOUND
    };
    while (cur_block) {
        while (try_merge_with_next(cur_block));
        result.block = cur_block;
        if (cur_block->is_free && block_is_big_enough(sz, cur_block)) {
            result.type = BSR_FOUND_GOOD_BLOCK;
            break;
        }
        cur_block = cur_block->next;
    }
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block){
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type == BSR_REACHED_END_NOT_FOUND) {return result;}
    split_if_too_big(result.block, query);
    result.block->is_free = false;
    return (struct block_search_result) {
        .block = result.block,
        .type = BSR_FOUND_GOOD_BLOCK
    };
}


static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    void* start_new_region = block_after(last);
    query = size_from_capacity((block_capacity) {query}).bytes;
    struct region new_region = alloc_region(start_new_region, query);
    if (region_is_invalid(&new_region)) return NULL;
    struct block_header* new_block = (struct block_header*) new_region.addr;
    last->next = new_block;
    if (try_merge_with_next(last)) {
        return last;
    }
    else {
        return new_block;
    }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    query = block_actual_capacity(query);
    struct block_search_result mapped_block_result = try_memalloc_existing(query, heap_start);
    if (mapped_block_result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header* new_last_block = grow_heap(mapped_block_result.block, query);
        if (!new_last_block) {return NULL;}
        mapped_block_result = try_memalloc_existing(query, new_last_block);
    }
    return mapped_block_result.block;
}

void* _malloc(size_t query) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START);
  if (addr) return addr->contents;
  else return NULL;
}
 
struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
  if (!mem) return;
  struct block_header* header = block_get_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header));
}
