#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

#include <inttypes.h>

#define TEST_COUNT 5
#define HEAP_INITIAL_SIZE (4096 * 3)

void before_test(uint8_t number, const char* const description, void* start_heap);
void after_test(uint8_t number, void* start_heap);

void test1_free_heap_more_that_query_when_map_block_successful(void* start_heap);
void test2_several_blocks_mapped_and_free_to_one_when_only_one_free(void* start_heap);
void test3_several_blocks_mapped_and_free_to_two_when_only_two_free(void* start_heap);
void test4_heap_not_enough_and_free_near_when_grow_heap_near_it(void* start_heap);
void test5_heap_not_enough_and_not_free_near_when_grow_heap_with_skip(void* start_heap);

#endif 
